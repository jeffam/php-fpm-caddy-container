# PHP-FPM Caddy Container

This container image can run PHP applications in a single container. I use it for simple, smallish, self-contained Drupal 8-11 sites.

It uses:

- The official [PHP-FPM alpine image](https://hub.docker.com/_/php/)
- [Caddy](https://caddyserver.com/) web server
- [Supervisor](https://github.com/Baldinof/caddy-supervisor/) Caddy plugin, to manage the PHP-FPM processes

It is assumed that containers running this image will do so behind a reverse proxy, so the port is hard-coded in `config/Caddyfile` (thus disabling auto-https) and the `trusted_proxies` setting is enabled.

[`apcu`](https://pecl.php.net/package/APCu) is included (mostly for Drupal), and PHP is compiled with `opcache`, `gd`, and `jpeg` support.

PHP, Caddy, and `apcu` are pinned to specific versions (rather than `latest`). See the `Containerfile` for details.

## Web root

By default, we web root is set to `/app/docroot`. This can be populated by extending this image and pre-populating it, or by creating a bind mount.

## Sending email

The Alpine Linux base that the PHP image uses includes BusyBox, which has a [`sendmail`](https://www.busybox.net/downloads/BusyBox.html#sendmail
) command that can relay mail to an SMTP server.

You can pass the following environment variables into the container to configure the command:

```
SMTP_USER
SMTP_PASS
SMTP_HOST (e.g. mail.example.net:587)
SMTP_SENDER
```

See the `php_admin_value[sendmail_path]` value in `config/zz-www.conf` for more info.

## Building

```
podman build -t php-fpm-caddy:8.1.13-build1 .
```

Replace `podman` with `docker` if using Docker.

## Example usage

In these examples:

`--init` is used to prevent zombie processes.
`--rm` is used to remove the container when stopped.

### Webroot in bind mount

```
podman run --init --name=php_app -p 9999:8181 -e SMTP_USER="user@example.com" -e SMTP_PASS="PASSWORD123" -e SMTP_HOST="mail.example.com:587" -e SMTP_SENDER="user@example.com" -d --rm -v=$(PWD)/docroot:/app/docroot registry.gitlab.com/jeffam/php-fpm-caddy:8.1.13
```

### Webroot in custom image

`Containerfile` (aka `Dockerfile`):

```
FROM registry.gitlab.com/jeffam/php-fpm-caddy:8.1.13

# Use .containerignore to skip files and directories.
# WORKDIR is set to `/app` so be sure that the current directory has a
# `docroot` directory.
COPY --chown=www:www . .

CMD ["/usr/bin/caddy", "run", "--config", "/etc/Caddyfile"]
```
